#!/usr/bin/python3

# Required dependencies
# pip3 install pyproj
# pip3 install numpy

from pyproj import Proj, transform
import numpy as np
import math
import sys
import getopt

# setup the projections
# choose the appropriate local projection depending on your
# operative location
locart = Proj(init='epsg:3003') # Italy
wgs84 = Proj(init='epsg:4326')

# dictonary containing the "enum"
# "lines" creates forward lines towards the direction
# "zigzag" creates a forward/backward towards the direction
pathTypeEnum = ['lines', 'zigzag']
dirTypeEnum = ['hor', 'ver']

def waypointsToFile(points, filename):
    cmd_index = 0
    def generateCommand(type, params=None):
        nonlocal cmd_index
        cmd_string = ""

        if(type == "HOME"):
            if params is None:
                print("[Error] Home command requires parameter")
                exit(-1)
            # PARAMS
            # delay (seconds)
            # latitude
            # longitude
            # altitude (meter)
            cmd_string = "{0}\t1\t0\t16\t{1}\t0\t0\t0\t{2}\t{3}\t{4}\t1".format(
                cmd_index, params[0], params[1], params[2], params[3])
        elif(type == "LOITER"):
            if params is None:
                print("[Error] Home command requires parameter")
                exit(-1)
            # PARAMS
            # delay (seconds)
            # latitude
            # longitude
            # altitude (meter)
            cmd_string = "{0}\t0\t3\t19\t{1}\t0\t1\t0\t{2}\t{3}\t{4}\t1".format(
                cmd_index, params[0], params[1], params[2], params[3])
        elif(type == "LAND"):
            cmd_string = "{0}\t0\t3\t21\t0\t0\t0\t0\t0\t0\t0\t1".format(
                cmd_index)
        elif(type == "WAYPOINT"):
            if params is None:
                print("[Error] Home command requires parameter")
                exit(-1)
            # PARAMS
            # delay (seconds)
            # latitude
            # longitude
            # altitude (meter)
            cmd_string = "{0}\t0\t3\t16\t{1}\t0\t0\t0\t{2}\t{3}\t{4}\t1".format(
                cmd_index, params[0], params[1], params[2], params[3])
        elif(type == "CYAW"):
            if params is None:
                print("[Error] Home command requires parameter")
                exit(-1)
            # PARAMS
            # amount of rotation (degrees)
            # rotation direction (1=CW, -1=CCW, 0=auto)
            # rotation type (0=absolute, 1=relative)
            cmd_string = "{0}\t0\t3\t115\t{1}\t0\t{2}\t{3}\t0\t0\t0\t1".format(
                cmd_index, params[0], params[1], params[2])
        elif(type == "CAMROT"):
            if params is None:
                print("[Error] Home command requires parameter")
                exit(-1)
            # PARAMS
            # pitch rotation (degrees)
            # roll rotation (degrees)
            # pan type (degrees)
            cmd_string = "{0}\t0\t3\t205\t{1}\t{2}\t{3}\t0\t0\t0\t0\t1".format(
                cmd_index, params[0], params[1], params[2])
        elif(type == "SHOT"):
            # PARAMS
            # the first param is the on/off command
            # the secondo one is the shutter command
            cmd_string = "{0}\t0\t3\t203\t{1}\t0\t0\t0\t{2}\t0\t0\t1".format(
                cmd_index, 1, 1)
        else:
            print("[Error] Unknown command, skip")
            exit(-1)
        # increase counter and return current command string
        cmd_index = cmd_index + 1
        return cmd_string

    # writing to file
    f = open(filename, "w+")
    print("[Info] Preparing the mission file {0}".format(filename))
    # header for each mission file
    f.write("QGC WPL 110\n")
    # the origin point is always the first one
    # and is set to be the home position
    str = generateCommand("HOME", (0, points[0][0], points[0][1], points[0][2]))
    f.write("{0}\n".format(str))
    # for each point send: move command, condyaw, control gimbal, shot
    for i in range(1,len(points)):
        str = generateCommand("WAYPOINT", (3, points[i][0], points[i][1], points[i][2]))
        f.write("{0}\n".format(str))
        str = generateCommand("CAMROT", (0, 0, 0))
        f.write("{0}\n".format(str))
        str = generateCommand("SHOT")
        f.write("{0}\n".format(str))
        str = generateCommand("CYAW", (270, 0, 0))
        f.write("{0}\n".format(str))
    # final land
    str = generateCommand("LOITER", (0, points[0][0], points[0][1], points[0][2]))
    f.write("{0}\n".format(str))
    str = generateCommand("LAND")
    f.write("{0}".format(str))

    f.close()
    print("[Info] Done")

def getSizeFromFov(fov, faceDistance, overlap):
    if overlap == 1.0:
        print("[Error] The required overlap is not applicable")
        exit(-1)

    frustumSize = ((faceDistance * math.tan(fov[0]/2.0)) * 2.0, (faceDistance * math.tan(fov[1]/2.0)) * 2.0)
    return (frustumSize[0] * (1-overlap), frustumSize[1] * (1-overlap))

def validateExtents(w, h, inc):
    # get the horizontal and vertical sampled size
    wElem = w/inc[0]
    hElem = h/inc[1]

    # check if are divisible
    if not wElem.is_integer() or not hElem.is_integer():
        print("[Warning] The subdivision requested is not possible, it will be rounded")
        return int(wElem), int(hElem)
    else:
        return wElem, hElem

def makeWaypoints(origin, end, height, gOffset, increment, pathType):
    # compute the initial point projected onto cartesian
    iX, iY = transform(wgs84, locart, origin[1], origin[0])
    # compute the final point projected onto cartesian
    fX, fY = transform(wgs84, locart, end[1], end[0])
    # compute the euclidean distance
    width = np.linalg.norm(np.array((iX, iY))- np.array((fX, fY)))
    if width == 0:
        print("[Error] The origin and the end coincides")
        exit(-1)
    else:
        print("[Info] The distance is " + str(width) + " meters")
    # compute the subdivision and if needed get the closest
    # integer subdivision
    hCount, vCount = validateExtents(width, height, increment)
    # compute the equispaced points composing the path
    offsets = []
    if(pathType[0] == pathTypeEnum[0]):
        if(pathType[1] == dirTypeEnum[0]):
            print("[Info] Forward lines along the horizontal direction")
            # forward lines along the horizontal direction
            for j in range(0, vCount):
                for i in range(0, hCount):
                    offsets.append((i*increment[0], j*increment[1]))
        elif (pathType[1] == dirTypeEnum[1]):
            print("[Info] Forward lines along the vertical direction")
            # forward lines along the vertical direction
            for i in range(0, hCount):
                for j in range(0, vCount):
                    offsets.append((i*increment[0], j*increment[1]))
        else:
            print("[Error] Inexistent path type and direction")
            exit(-1)
    elif pathType[0] == pathTypeEnum[1]:
        if(pathType[1] == dirTypeEnum[0]):
            print("[Info] Alternating lines along the horizontal direction")
            # forward/backward lines along the horizontal direction
            for j in range(0, vCount):
                for i in range(0, hCount):
                    if (j+1) % 2 == 0 and j > 0:
                        offsets.append((((hCount - 1)*increment[0]) - (i*increment[0]), j*increment[1]))
                    else:
                        offsets.append((i*increment[0], j*increment[1]))
        elif (pathType[1] == dirTypeEnum[1]):
            print("[Info] Alternating lines along the vertical direction")
            # forward/backward lines along the horizontal direction
            for i in range(0, hCount):
                for j in range(0, vCount):
                    if (i+1) % 2 == 0 and i > 0:
                        offsets.append((i*increment[0], ((vCount - 1)*increment[1]) - (j*increment[1])))
                    else:
                        offsets.append((i*increment[0], j*increment[1]))
        else:
            print("[Error] Inexistent path type and direction")
            exit(-1)
    else:
        print("[Error] Inexistent path type and direction")
        exit(-1)
    # compute the waypoint list
    waypoints = []
    print("[Info] Converting to wgs84...")
    for i in range(len(offsets)):
        v_dir = np.array((fX, fY) - np.array((iX, iY)))
        h_dir = v_dir/np.linalg.norm(v_dir)

        coords = np.array((iX, iY)) + (offsets[i][0] * h_dir)
        lng, lat = transform(locart, wgs84, coords[0], coords[1])
        waypoints.append((lat, lng, offsets[i][1] + gOffset))
        print("[Info] Point {0}\{1}".format(i, len(offsets)), end="\r")

    # print resume
    print("[Info] Generation compled! Resume:")
    print("[Info] Origin (lat/lng): {0}".format(origin))
    print("[Info] Final (lat/lng): {0}".format(end))
    print("[Info] Steps: {0}".format(increment))
    print("[Info] Extents (meters): {0:}".format((round(width, 2), round(height,2))))
    print("[Info] Horizontal points: {0}".format(hCount))
    print("[Info] Vertical points: {0}".format(vCount))
    print("[Info] Number of waypoints {0}".format(len(waypoints)))

    return waypoints

def usage():
    print("Usage: main.py -zlvhf -o <filename> --parameters")
    print("\n-o Set the output filename (default: output.waypoint)")
    print("-zl Set the path type (zig/zag or lines) (default: lines)")
    print("-vh Set the direction of the path (vertical or horizontal) (default: horizontal)")
    print("-f The step is defined using the field of view (default: step)")
    print("\nAll the following parameters are mandatory\n")
    print("--height Set the face height w.r.t the ground offset (height:offset in meter)")
    print("--step Set the horizontal and vertical step (hStep:vStep in meter)")
    print("--fov Set the camera FOV (hFov:vFov in degrees)")
    print("--dist Set the acquisition distance (meter)")
    print("--overlap Set the desired overlap (percentage)")
    print("--initial Set the latitude and the longitude of the origin (lat:lng)")
    print("--final Set the latitude and the longitude of of the end (lat:lng)")

def main():
    ## TESTING
    # (45.411452, 10.400824)
    # (45.411222, 10.401398)

    # the initial point of the face (in lat/lng)
    iPointLatLng = (0, 0)
    # the final point of the face (in lat/lng)
    fPointLatLng = (0, 0)
    # the height of the face and the ground offset (in meter)
    faceHeight = 0
    faceGroundOffset = 0
    # the horizontal and vertical sampling step (in meter)
    step = (0, 0)
    # camera field on view, face distance and overlap
    fov = (0, 0)
    faceDistance = 0
    overlap = 0.5
    # specify the type
    pType = (pathTypeEnum[0], dirTypeEnum[0])
    # default output filename
    outputFile = "output.waypoint"

    useFov = False
    paramFilled = 0
    
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "zlvhfo:", [
            "help", "initial=", "final=", "height=", "step=", "fov=", "dist=", "overlap="])
    except getopt.GetoptError as err:
        print(err)
        usage()
        exit(2)

    if len(opts) != 0:
        for (opt, arg) in opts:
            # help
            if opt == "--help":
                usage()
                exit(0)

            # check if the step must be computed using the fov
            if opt == "-f":
                useFov = True

            # output filename
            if opt == "-o":
                outputFile = arg

            # path type definition
            if opt == "-z":
                tmpList = list(pType)
                tmpList[0] = pathTypeEnum[1]
                pType = tuple(tmpList)
            if opt == "-l":
                tmpList = list(pType)
                tmpList[0] = pathTypeEnum[0]
                pType = tuple(tmpList)
            
            # vertical or horizontal direction definition
            if opt == "-h":
                tmpList = list(pType)
                tmpList[1] = dirTypeEnum[0]
                pType = tuple(tmpList)
            if opt == "-v":
                tmpList = list(pType)
                tmpList[1] = dirTypeEnum[1]
                pType = tuple(tmpList)

            # path coordinates
            if opt == "--initial":
                slat_lng = arg.split(':')
                tmpList = list(iPointLatLng)
                tmpList[0] = float(slat_lng[0])
                tmpList[1] = float(slat_lng[1])
                iPointLatLng = tuple(tmpList)
                paramFilled += 1
            if opt == "--final":
                slat_lng = arg.split(':')
                tmpList = list(fPointLatLng)
                tmpList[0] = float(slat_lng[0])
                tmpList[1] = float(slat_lng[1])
                fPointLatLng = tuple(tmpList)
                paramFilled += 1
            if opt == "--step":
                shor_ver = arg.split(':')
                tmpList = list(step)
                tmpList[0] = float(shor_ver[0])
                tmpList[1] = float(shor_ver[1])
                step = tuple(tmpList)
                paramFilled += 1
            if opt == "--fov":
                shor_ver = arg.split(':')
                tmpList = list(fov)
                tmpList[0] = math.radians(float(shor_ver[0]))
                tmpList[1] = math.radians(float(shor_ver[1]))
                fov = tuple(tmpList)
                paramFilled += 1
            if opt == "--dist":
                faceDistance = float(arg)
                paramFilled += 1
            if opt == "--overlap":
                overlap = float(arg)/100.0
                paramFilled += 1
            if opt == "--height":
                shei_off = arg.split(':')
                faceHeight = float(shei_off[0])
                faceGroundOffset = float(shei_off[1])
                paramFilled += 1
    
    if useFov and paramFilled == 6:
        print("[Info] Step will be calculated with fov {0}, distance {1} with {2} overlap".format(
            fov, faceDistance, overlap))
        step = getSizeFromFov(fov, faceDistance, overlap)
        print("[Info] Step is {0}".format(step))
    elif not useFov and paramFilled >= 4:
        print("[Info] Step is defined manually {0}".format(step))
    else:
        print("[Error] Missing parameters: {0} provided".format(paramFilled))
        exit(-1)

    waypoints = makeWaypoints(iPointLatLng, fPointLatLng, faceHeight, faceGroundOffset, step, pType)
    waypointsToFile(waypoints, outputFile)

if __name__ == '__main__':
    main()
