# Usage
Usage `main.py -zlvh -o <filename> --parameters`
## Options
- **-o** set the output filename (default: output.waypoint)
- **-zl** set the path type (zig/zag or lines) (default: lines)
- **-vh** set the direction of the path (vertical or horizontal) (default: horizontal)
- **-f** the step is defined using the field of view (default: step)
## Parameters
The following parameters are mandatory
- **--height** set the face height w.r.t the ground offset (height:offset in meter)
- **--initial** set the latitude and the longitude of the origin (lat:lng)
- **--final** set the latitude and the longitude of the of the end (lat:lng)

if the option `-f` is not defined is mandatory to set the following parameter
- **--step** set the horizontal and vertical step (hStep:vStep in meter)

in the other case you must define the following parameters in order to use the automatic step
- **--fov** set the camera field of view (hFov:vFov in degrees)
- **--dist** set the acquisition distance (meter)
- **--overlap** set the desired overlap (percentage)
