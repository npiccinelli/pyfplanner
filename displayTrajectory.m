close all

%coords = hl(hl(:,4) == 16,9:11);
%coords = hz(hz(:,4) == 16,9:11);
%coords = vl(vl(:,4) == 16,9:11);
%coords = vz(vz(:,4) == 16,9:11);

coords = sab13(sab13(:,4) == 16,9:11);

[xNorth,yEast,zDown] = geodetic2ned(coords(:,1),coords(:,2),coords(:,3),coords(1,1),coords(1,2),coords(1,3),wgs84Ellipsoid);
plot3(xNorth,yEast,zDown,'*');
for i=1:length(coords)
    text(xNorth(i),yEast(i),zDown(i),['   ', num2str(i)],'HorizontalAlignment','left','FontSize',8);
end